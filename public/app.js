function urlB64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');
  
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
  
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }
  
  const applicationServerPublicKey  = 'BBAOC6tBY7XDXEtppq0otDqyERelAsO3D_odVRGpukfpmbVq3exnsM3AoehG52-T-BDsAeJS6db2oy5jfDJt8iI';
  const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
  let isSubscribed = false;
  let swRegistration  = null;
  
  if ('serviceWorker' in navigator && 'PushManager' in window) {
    navigator.serviceWorker.register('/serviceWorker.js')
    .then(function(swReg) {
	  console.log(swReg);

      swRegistration = swReg;
      initialiseUI();
  
    })
    .catch(function(error) {
      console.log(error)
    });
  } else {
    pushButton.textContent = 'Push Not Supported';
  }
  
  var pushButton  = document.getElementById('subscribeBtn')
  
  function initialiseUI() {
    pushButton.addEventListener('click', function() {
      pushButton.disabled = true;
      if (isSubscribed) {
        unsubscribeUser();
      } else {
        subscribeUser();
      }
    });
  
    swRegistration.pushManager.getSubscription()
    .then(function(subscription) {
      isSubscribed = !(subscription === null);
  
      updateSubscriptionOnServer(subscription);
  
      updateBtn();
    });
  }
  
  function subscribeUser() {
    const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
    swRegistration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: applicationServerKey
    })
    .then(function(subscription) {
      updateSubscriptionOnServer(subscription);
  
      isSubscribed = true;
  
      updateBtn();
    })
    .catch(function(err) {
      updateBtn();
    });
  }
  
  function unsubscribeUser() {
    swRegistration.pushManager.getSubscription()
    .then(function(subscription) {
      if (subscription) {
        return subscription.unsubscribe();
      }
    })
    .catch(function(error) {
      console.log('Error unsubscribing', error);
    })
    .then(function() {
      updateSubscriptionOnServer(null);
  
      isSubscribed = false;
  
      updateBtn();
    });
  }
  
  function updateSubscriptionOnServer(subscription) {
    if (subscription) {
		// Simpan data subcription (endpoint, keys) ke db sesuai id yg login
		// NOTE: Ini cuma test
		$('#isActive').show();
		$('#isActive').text(JSON.stringify(subscription));
      } else {
		// Hapus data subcriptiondari db sesuai id yg login
		// NOTE: Ini cuma test
        $('#isActive').hide();
		$('#isActive').text('');
      }
  }
  
  function updateBtn() {
    if (Notification.permission === 'denied') {
      pushButton.textContent = 'Push Messaging Blocked.';
      pushButton.disabled = true;
      updateSubscriptionOnServer(null);
      return;
    }
  
    if (isSubscribed) {
      $('#pushBtn').text('Aktif')
    } else {
      $('#pushBtn').text('Nonaktif')
    }
  
    pushButton.disabled = false;
  }