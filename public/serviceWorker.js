var CACHE_NAME = "Laravel-Cache-PWA-v1"

var urlsToCache = [
  '/favicon.ico',
];

self.addEventListener('install', function(evt) {
  evt.waitUntil(
      caches.open(CACHE_NAME).then((cache) => {
        return cache.addAll(urlsToCache);
      })
  );
})

self.addEventListener('activate', function(event) {
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.filter(function(cacheName) {
        }).map(function(cacheName) {
          return caches.delete(cacheName);
        })
      );
    })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(fetch(event.request));
});

self.addEventListener('push', function(event) {
  const title = 'Push Laravel';
  const options = {
    body: `${event.data.text()}`,
    icon: '/assets/img/config/hokan-152.png',
    badge: '/assets/img/config/hokan-152.png'
  };

  event.waitUntil(self.registration.showNotification(title, options));
});
